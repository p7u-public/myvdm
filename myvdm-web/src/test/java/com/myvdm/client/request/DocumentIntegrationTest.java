package com.myvdm.client.request;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.myvdm.server.domain.Document;
import com.myvdm.server.domain.DocumentType;
import com.myvdm.server.domain.Field;
import com.myvdm.server.domain.StringFieldValue;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.validation.ConstraintViolation;
import org.mockito.ArgumentCaptor;
import org.testng.annotations.Test;
import static org.testng.Assert.*;
import static org.mockito.Mockito.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.jpa.EntityManagerHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.testng.annotations.BeforeClass;

/**
 *
 * @author Razvan Petruescu
 */
//@Test
public class DocumentIntegrationTest {

    final Logger logger = LoggerFactory.getLogger(DocumentIntegrationTest.class);

    private ApplicationRequestFactory requestFactory;

    @BeforeClass
    public void setUp() {
        ConfigurableApplicationContext context =
                new ClassPathXmlApplicationContext("classpath:META-INF/spring/applicationContext-test.xml");

        EntityManagerFactory emf = (EntityManagerFactory) context.getBean("entityManagerFactory");

        EntityManager em = emf.createEntityManager();
        TransactionSynchronizationManager.bindResource(emf, new EntityManagerHolder(em));
        // Non-managed environment idiom
        EntityTransaction transaction = null;
        try {
            transaction = em.getTransaction();
            transaction.begin();

            //*/
            Document document = new Document();
            document.setName("myDocument");
            em.persist(document);

            DocumentType documentType = new DocumentType();
            documentType.setName("myDocumentType");
            em.persist(documentType);

            // TODO I cannot persist IFV
//        IntegerFieldValue integerFieldValue = new IntegerFieldValue();
//        integerFieldValue.setValue(123);
//        em.persist(integerFieldValue);

            Field stringField = new Field();
            stringField.setName("myField1");
            StringFieldValue stringFieldValue = new StringFieldValue();
            stringFieldValue.setValue("sfv1");
            stringField.setValue(stringFieldValue);
            em.persist(stringField);

            Field stringField2 = new Field();
            stringField2.setName("myField2");
            StringFieldValue stringFieldValue2 = new StringFieldValue();
            stringFieldValue2.setValue("sfv2");
            stringField2.setValue(stringFieldValue2);
            em.persist(stringField2);

            Document documentWithChildren = new Document();
            documentWithChildren.setName("myDocumentWithChildren");
            documentWithChildren.setType(documentType);
            documentWithChildren.setFields(Lists.newArrayList(stringField, stringField2));
            em.persist(documentWithChildren);
            //*/
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            throw e; // or display error message
        } finally {
            try {
                em.close();
            } catch (Exception e) {
                // note that em is still not closed
            }
        }

        /*
        EntityManager em2 = emf.createEntityManager();
        try {
            EntityTransaction transaction2 = em2.getTransaction();
            transaction2.begin();

            stringFieldValue.setValue("mut");
            em2.persist(document);

            transaction2.commit();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        */
        
        requestFactory = IntegrationTestHelper.create(ApplicationRequestFactory.class);
    }

    public void shouldSaveDocument() {

        DocumentRequestContext saveContext = requestFactory.documentRequestContext();
        DocumentProxy documentProxy = saveContext.create(DocumentProxy.class);
        documentProxy.setName("test-created-document");

        //save document
        Receiver<Void> saveReceiver = mock(Receiver.class);
        saveContext.persist().using(documentProxy).fire(saveReceiver);
        verify(saveReceiver).onSuccess(any(Void.class));


        /*
         final DocumentTypeRequestContext documentTypeRequestContext = requestFactory.documentTypeRequestContext();
         final DocumentTypeProxy documentType = documentTypeRequestContext.create(DocumentTypeProxy.class);
         documentType.setName("test-created-document-type");
        
        
         //save document type
         Receiver<Void> documentTypeReceiver = mock(Receiver.class);
         documentTypeRequestContext.persist().using(documentType).fire(documentTypeReceiver);
         verify(documentTypeReceiver).onSuccess(any(Void.class));
         */

        //retrieve
        DocumentRequestContext retrieveContext = requestFactory.documentRequestContext();
        @SuppressWarnings("unchecked")
        final Receiver retrievalReceiver = mock(Receiver.class);
        retrieveContext.find(documentProxy.stableId()).fire(retrievalReceiver);


        // check
        ArgumentCaptor<DocumentProxy> captor = ArgumentCaptor.forClass(DocumentProxy.class);

        verify(retrievalReceiver).onSuccess(captor.capture());
        assertEquals("test-created-document", captor.getValue().getName());
    }

    public void shouldUpdatePrepopulatedDocumentWithNewName() {

        /*----- find pre-populated document & edit & persist again -----*/
        DocumentRequestContext retrievalContext = requestFactory.documentRequestContext();

        final Receiver<List<DocumentProxy>> retrievalReceiver = new Receiver<List<DocumentProxy>>() {
            @Override
            public void onSuccess(List<DocumentProxy> response) {
                DocumentProxy document = response.get(0);
                assertEquals(document.getName(), "myDocument");

                final DocumentRequestContext documentRequestContext = requestFactory.documentRequestContext();
                document = documentRequestContext.edit(document);
                document.setName("edited-name");

                Receiver<Void> receiver = mock(Receiver.class);
                documentRequestContext.persist().using(document).fire(receiver);
                verify(receiver).onSuccess(any(Void.class));
            }
        };
        retrievalContext.findDocumentEntries(0, 50).fire(retrievalReceiver);
        /*----- END -----*/

        /*
         DocumentRequestContext retrievalContext2 = requestFactory.documentRequestContext();
         @SuppressWarnings("unchecked")
         final Receiver<DocumentProxy> retrievalReceiver2 = new Receiver<DocumentProxy>() {
         @Override
         public void onSuccess(DocumentProxy response) {
         assertEquals(response.getName(), "edited-name");
         }
         };
         retrievalContext2.find((EntityProxyId<DocumentProxy>) document.stableId()).fire(retrievalReceiver2);
         */

    }

    public void shouldUpdateAnyPrepopulatedDocumentWithPrepopulatedType() {

        DocumentTypeRequestContext requestContext = requestFactory.documentTypeRequestContext();
        final Receiver<List<DocumentTypeProxy>> typeRetrievalReceiver = new Receiver<List<DocumentTypeProxy>>() {
            @Override
            public void onSuccess(List<DocumentTypeProxy> response) {
                final DocumentTypeProxy type = response.get(0);
                assertEquals(type.getName(), "myDocumentType");

                /*----- find any document & edit & persist again -----*/
                DocumentRequestContext retrievalContext = requestFactory.documentRequestContext();

                final Receiver<List<DocumentProxy>> retrievalReceiver = new Receiver<List<DocumentProxy>>() {
                    @Override
                    public void onSuccess(List<DocumentProxy> response) {
                        DocumentProxy document = response.get(0);

                        final DocumentRequestContext documentRequestContext = requestFactory.documentRequestContext();
                        document = documentRequestContext.edit(document);
                        document.setType(type);

                        Receiver<Void> receiver = mock(Receiver.class);
                        documentRequestContext.persist().using(document).fire(receiver);
                        verify(receiver).onSuccess(any(Void.class));
                    }
                };
                retrievalContext.findDocumentEntries(0, 50).fire(retrievalReceiver);
                /*----- END -----*/
            }
        };
        requestContext.findDocumentTypeEntries(0, 10).fire(typeRetrievalReceiver);
    }

    public void shouldUpdateAnyPrepopulatedDocumentWithPrepopulatedField() {

        FieldRequestContext requestContext = requestFactory.fieldRequestContext();
        final Receiver<List<FieldProxy>> fieldRetrievalReceiver = new Receiver<List<FieldProxy>>() {
            @Override
            public void onSuccess(List<FieldProxy> response) {
                final FieldProxy field = response.get(0);
                assertEquals(field.getName(), "myField1");

                /*----- find any document & edit & persist again -----*/
                DocumentRequestContext retrievalContext = requestFactory.documentRequestContext();

                final Receiver<List<DocumentProxy>> retrievalReceiver = new Receiver<List<DocumentProxy>>() {
                    @Override
                    public void onSuccess(List<DocumentProxy> response) {
                        DocumentProxy document = response.get(0);

                        final DocumentRequestContext documentRequestContext = requestFactory.documentRequestContext();
                        document = documentRequestContext.edit(document);
                        document.setFields(Collections.singletonList(field));

                        Receiver<Void> receiver = mock(Receiver.class);
                        documentRequestContext.persist().using(document).fire(receiver);
                        verify(receiver).onSuccess(any(Void.class));
                    }
                };
                retrievalContext.findDocumentEntries(0, 50).fire(retrievalReceiver);
                /*----- END -----*/
            }
        };
        requestContext.findFieldEntries(0, 10).fire(fieldRetrievalReceiver);

    }

    public void shouldUpdateAnyPrepopulatedDocumentWithNewField() {

        /*----- find any document & edit & persist again -----*/
        DocumentRequestContext retrievalContext = requestFactory.documentRequestContext();

        retrievalContext.findDocumentEntries(0, 50).fire(new Receiver<List<DocumentProxy>>() {
            @Override
            public void onSuccess(List<DocumentProxy> response) {
                DocumentProxy document = response.get(0);

                final DocumentRequestContext documentRequestContext = requestFactory.documentRequestContext();
                document = documentRequestContext.edit(document);

                final FieldRequestContext fieldRequestContext = requestFactory.fieldRequestContext();
                StringFieldValueProxy fieldValueProxy = fieldRequestContext.create(StringFieldValueProxy.class);
                fieldValueProxy.setValue("value");
                FieldProxy fieldProxy = fieldRequestContext.create(FieldProxy.class);
                fieldProxy.setName("name");
                fieldProxy.setValue(fieldValueProxy);
                document.setFields(Collections.singletonList(fieldProxy));

                Receiver<Void> receiver = mock(Receiver.class);
                documentRequestContext.persist().using(document).fire(receiver);
                verify(receiver).onSuccess(any(Void.class));
                /*
                 */
            }
        });
        /*----- END -----*/
    }

    // This one crashes due to DN bug, so it is disabled
    @Test(enabled = false)
    public void shouldUpdatePrepopulatedDocumentFieldsWithEditedValues() {
        DocumentRequestContext retrievalContext = requestFactory.documentRequestContext();

        retrievalContext.findDocumentEntries(0, 10).with("*.*").fire(new Receiver<List<DocumentProxy>>() {
            @Override
            public void onSuccess(List<DocumentProxy> response) {
                DocumentProxy document = response.get(1);
                final DocumentRequestContext documentRequestContext = requestFactory.documentRequestContext();
                document = documentRequestContext.edit(document);

                final List<FieldProxy> fields = document.getFields();
                final FieldProxy field = fields.get(0);
                assertEquals(field.getName(), "myField1");
                final FieldValueProxy value = field.getValue();
                final StringFieldValueProxy sfvp = (StringFieldValueProxy) value;
                assertEquals(sfvp.getValue(), "sfv1");


                sfvp.setValue("mutated");
                //Receiver<Void> receiver = mock(Receiver.class);
                final MockReceiver mockReceiver = new MockReceiver();
                documentRequestContext.persist().using(document).fire(mockReceiver);
                //verify(receiver).onSuccess(any(Void.class));

            }
        });
    }

    public void shouldRetrievePrepopulatedDocumentWithAllChildren() {

        DocumentRequestContext retrievalContext = requestFactory.documentRequestContext();

        final Receiver<List<DocumentProxy>> retrievalReceiver = new Receiver<List<DocumentProxy>>() {
            @Override
            public void onSuccess(List<DocumentProxy> response) {
                final DocumentProxy documentWithChildren = response.get(1);
                assertEquals(documentWithChildren.getName(), "myDocumentWithChildren");
                assertTrue(documentWithChildren.getType() != null, "Type is null !! ");
                assertEquals(documentWithChildren.getType().getName(), "myDocumentType");
                assertTrue(documentWithChildren.getFields() != null, "Fields are null !! ");
                assertEquals(documentWithChildren.getFields().size(), 2);
                for (FieldProxy fieldProxy : documentWithChildren.getFields()) {
                    assertTrue(fieldProxy.getValue() != null, "Field value is null !!");
                    final FieldValueProxy fvp = fieldProxy.getValue();
                    StringFieldValueProxy sfvp = (StringFieldValueProxy) fvp;
                    assertTrue(sfvp.getValue() != null, "SFVP has no value !!");
                }
            }
        };
        //retrievalContext.findDocumentEntries(0, 50).with("type", "fields", "fields.value").fire(retrievalReceiver);
        retrievalContext.findDocumentEntries(0, 50).with("*.*").fire(retrievalReceiver);

    }

    public final void shouldDeleteDocument() {

        DocumentRequestContext saveContext = requestFactory.documentRequestContext();
        DocumentProxy documentProxy = saveContext.create(DocumentProxy.class);
        documentProxy.setName("test-created-document");

        //save document
        Receiver<Void> saveReceiver = mock(Receiver.class);
        saveContext.persist().using(documentProxy).fire(saveReceiver);
        verify(saveReceiver).onSuccess(any(Void.class));


        DocumentRequestContext deleteContext = requestFactory.documentRequestContext();
        @SuppressWarnings("unchecked")
        final Receiver<Void> deleteReceiver = mock(Receiver.class);
        deleteContext.remove().using(documentProxy).fire(deleteReceiver);
        verify(deleteReceiver).onSuccess(any(Void.class));
    }

    // Disabled due to crash due to DN versio upgrade, check out !
    @Test(enabled = false)
    public final void shouldFailWithValidationError() {
        DocumentRequestContext saveContext = requestFactory.documentRequestContext();
        DocumentProxy document = saveContext.create(DocumentProxy.class);

        //save document
        @SuppressWarnings("unchecked")
        final Receiver<Void> saveReceiver =  new MockReceiver() /*mock(Receiver.class)*/;
        saveContext.persist().using(document).fire(saveReceiver);

        verify(saveReceiver).onConstraintViolation(anySet());
    }

    private class MockReceiver extends Receiver<Void> {

        @Override
        public void onSuccess(Void response) {
            logger.info("On Success -> {}", response);
        }

        @Override
        public void onFailure(ServerFailure error) {
            logger.info("On Failure -> {} ~ Fatal? {}", error.getMessage(), error.isFatal());
            super.onFailure(error);
        }

        @Override
        public void onConstraintViolation(Set<ConstraintViolation<?>> violations) {
            logger.info("On Constraint Violation # property -> {}", Iterables.get(violations, 0).getPropertyPath());
            logger.info("On Constraint Violation # message -> {}", Iterables.get(violations, 0).getMessage());
        }
    }

}
