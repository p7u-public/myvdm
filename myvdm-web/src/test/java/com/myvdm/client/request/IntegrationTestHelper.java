package com.myvdm.client.request;

import com.google.web.bindery.event.shared.SimpleEventBus;
import com.google.web.bindery.requestfactory.server.ServiceLayer;
import com.google.web.bindery.requestfactory.server.SimpleRequestProcessor;
import com.google.web.bindery.requestfactory.server.testing.InProcessRequestTransport;
import com.google.web.bindery.requestfactory.shared.RequestFactory;
import com.google.web.bindery.requestfactory.shared.RequestTransport;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.google.web.bindery.requestfactory.vm.RequestFactorySource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Razvan Petruescu
 */
public class IntegrationTestHelper {

    /**
     * Creates a {@link RequestFactory}.
     */
    public static <T extends RequestFactory> T create(Class<T> requestFactoryClass) {
        T factory = RequestFactorySource.create(requestFactoryClass);
        factory.initialize(new SimpleEventBus(), new InProcessRequestTransport(new SimpleRequestProcessor(ServiceLayer.create())));
        return factory;
    }
}

class HttpPostTransport implements RequestTransport {

    final static Logger logger = LoggerFactory.getLogger(IntegrationTestHelper.class);

    private final String urlToGwtServlet;

    public HttpPostTransport(String urlToGwtServlet) {
        this.urlToGwtServlet = urlToGwtServlet;
    }

    @Override
    public void send(String payload, RequestTransport.TransportReceiver receiver) {
        try {
            logger.info("<send> payload:: {}, receiver:: {}", payload, receiver);
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost post = createPostMethod(payload);
            final HttpResponse result = httpclient.execute(post);
            handleResult(result, post, receiver);
            post.releaseConnection();
        } catch (Exception e) {
            logger.info("<send> exception caught:: {}", e);
            receiver.onTransportFailure(new ServerFailure(e.getMessage(), e.getClass().getName(), "",
                    true));
        }
    }

    private HttpPost createPostMethod(String payload) throws UnsupportedEncodingException {
        logger.info("<createPostMethod> payload:: {}", payload);
        HttpPost post = new HttpPost(urlToGwtServlet);
        ContentType contentType = ContentType.create(RequestFactory.JSON_CONTENT_TYPE_UTF8);
        HttpEntity requestEntity = new StringEntity(payload, contentType);
        post.setEntity(requestEntity);
        return post;
    }

    private void handleResult(HttpResponse result, HttpPost post, RequestTransport.TransportReceiver receiver) throws IOException {
        logger.info("<handleResult> result:: {}, post:: {}, receiver:: {}", result, post, receiver);
        if (result.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            receiver.onTransportSuccess(/*post.getResponseBodyAsString()*/"WTF is this ???");
        } else {
            receiver.onTransportFailure(new ServerFailure("Server returned " + result));
        }
    }
}
