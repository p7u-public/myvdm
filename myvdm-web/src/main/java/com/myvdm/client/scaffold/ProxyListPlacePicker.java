package com.myvdm.client.scaffold;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.HasConstrainedValue;
import com.myvdm.client.place.ProxyListPlace;
import com.myvdm.client.place.ProxyPlaceToListPlace;

/**
 * Drives a {@link ValueChangeHandler} populated with {@link ProxyListPlace}
 * instances, keeping it in sync with the {@link PlaceController}'s notion of
 * the current place, and firing place change events as selections are made
 * 
 * @author razvan
 */
public class ProxyListPlacePicker implements ValueChangeHandler<Place>, PlaceChangeEvent.Handler {

    private HasConstrainedValue<Place> view;

    private final PlaceController placeController;

    private final ProxyPlaceToListPlace proxyPlaceToListPlace;

    public ProxyListPlacePicker(PlaceController placeController, ProxyPlaceToListPlace proxyPlaceToListPlace) {
        this.placeController = placeController;
        this.proxyPlaceToListPlace = proxyPlaceToListPlace;
    }

    @Override
    public void onPlaceChange(PlaceChangeEvent event) {
        view.setValue(proxyPlaceToListPlace.proxyPlaceFor(event.getNewPlace()), false);
    }

    @Override
    public void onValueChange(ValueChangeEvent<Place> event) {
        placeController.goTo(event.getValue());
    }

    public HandlerRegistration register(EventBus eventBus, HasConstrainedValue<Place> view) {
        this.view = view;
        final HandlerRegistration placeRegistration = eventBus.addHandler(PlaceChangeEvent.TYPE, this);
        final HandlerRegistration viewRegistration = view.addValueChangeHandler(this);

        return new HandlerRegistration() {
            @Override
            public void removeHandler() {
                placeRegistration.removeHandler();
                viewRegistration.removeHandler();
                ProxyListPlacePicker.this.view = null;
            }
        };
    }
}
