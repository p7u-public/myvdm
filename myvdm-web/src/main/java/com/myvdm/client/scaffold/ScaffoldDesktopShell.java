package com.myvdm.client.scaffold;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.place.shared.Place;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasConstrainedValue;
import com.google.gwt.user.client.ui.NotificationMole;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.ValuePicker;
import com.google.gwt.user.client.ui.Widget;
import com.myvdm.client.place.ProxyListPlace;

/**
 *
 * @author razvan
 */
public class ScaffoldDesktopShell extends Composite {

    interface Binder extends UiBinder<Widget, ScaffoldDesktopShell> {
    }

    private static final Binder BINDER = GWT.create(Binder.class);

    @UiField
    SimplePanel details;

    @UiField
    DivElement error;

    @UiField
    SimplePanel master;

    @UiField
    NotificationMole mole;

    @UiField(provided = true)
    ValuePicker<Place> placesBox = new ValuePicker<Place>(new AbstractRenderer<Place>() {

        @Override
        public String render(Place object) {
            return object instanceof ProxyListPlace ? "Lucrari" : "Cauta in Scopus";
        }
    });

    public ScaffoldDesktopShell() {
        initWidget(BINDER.createAndBindUi(this));
    }

    public SimplePanel getDetails() {
        return details;
    }

    public DivElement getError() {
        return error;
    }

    public SimplePanel getMaster() {
        return master;
    }

    public NotificationMole getMole() {
        return mole;
    }

    public HasConstrainedValue<Place> getPlacesBox() {
        return placesBox;
    }
}
