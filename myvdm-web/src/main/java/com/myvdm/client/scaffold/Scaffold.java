package com.myvdm.client.scaffold;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.myvdm.client.ioc.DesktopInjectorWrapper;
import com.myvdm.client.ioc.InjectorWrapper;

/**
 *
 * @author razvan
 */
public class Scaffold implements EntryPoint {

    final private InjectorWrapper injectorWrapper = GWT.create(DesktopInjectorWrapper.class);

    @Override
    public void onModuleLoad() {
        /*
         * Get and run platform specific app
         */
        injectorWrapper.getInjector().getScaffoldApp().run();
    }
}