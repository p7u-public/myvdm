package com.myvdm.client.scaffold;

import com.google.common.collect.ImmutableSet;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.logging.client.LogConfiguration;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasConstrainedValue;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.gwt.client.RequestFactoryLogHandler;
import com.google.web.bindery.requestfactory.shared.EntityProxyId;
import com.google.web.bindery.requestfactory.shared.LoggingRequest;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.myvdm.client.activity.ApplicationDetailsActivitiesMapper;
import com.myvdm.client.activity.ApplicationMasterActivitiesMapper;
import com.myvdm.client.activity.DocumentDetailsActivity;
import com.myvdm.client.activity.EmptyActivity;
import com.myvdm.client.activity.FindAndEditProxyActivity;
import com.myvdm.client.activity.uvt.BookActivity;
import com.myvdm.client.activity.uvt.ConferenceVolumeActivity;
import com.myvdm.client.activity.uvt.MagazineActivity;
import com.myvdm.client.event.CreateEvent;
import com.myvdm.client.event.CreateEventHandler;
import com.myvdm.client.event.DeleteEvent;
import com.myvdm.client.event.DeleteEventHandler;
import com.myvdm.client.event.DetailsEvent;
import com.myvdm.client.event.DetailsEventHandler;
import com.myvdm.client.event.EditEvent;
import com.myvdm.client.event.EditEventHandler;
import com.myvdm.client.place.ApplicationPlace;
import com.myvdm.client.request.ApplicationRequestFactory;
import com.myvdm.client.request.DocumentProxy;
import com.myvdm.client.request.RequestEvent;
import com.myvdm.client.place.ProxyListPlace;
import com.myvdm.client.place.ProxyPlace;
import com.myvdm.client.place.ProxyPlaceToListPlace;
import com.myvdm.client.place.ActivityAwarePlace;
import com.myvdm.client.request.DocumentRequestContext;
import com.myvdm.client.ui.DocumentEditView;
import com.myvdm.client.ui.ProxyDetailsView;
import com.myvdm.client.ui.ProxyEditView;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author razvan
 */
public class ScaffoldDesktopApp extends ScaffoldApp {

    private static final Logger log = Logger.getLogger(Scaffold.class.getName());
    private final ScaffoldDesktopShell shell;
    private final ApplicationRequestFactory requestFactory;
    private final EventBus eventBus;
    private final PlaceController placeController;
//    private final PlaceHistoryFactory placeHistoryFactory;
    private final ApplicationMasterActivitiesMapper masterActivitiesMapper;
    private final ApplicationDetailsActivitiesMapper detailsActivitiesMapper;
    private final ProxyDetailsView<DocumentProxy> detailsView;
    private final ProxyEditView<DocumentProxy, DocumentEditView> editView;

    @Inject
    public ScaffoldDesktopApp(
            ScaffoldDesktopShell shell,
            ApplicationRequestFactory requestFactory,
            EventBus eventBus,
            PlaceController placeController,
            /*
             * PlaceHistoryFactory placeHistoryFactory,
             */
            ApplicationMasterActivitiesMapper masterActivitiesMapper,
            ApplicationDetailsActivitiesMapper detailsActivitiesMapper,
            ProxyDetailsView<DocumentProxy> detailsView,
            ProxyEditView<DocumentProxy, DocumentEditView> editView) {
        this.shell = shell;
        this.requestFactory = requestFactory;
        this.eventBus = eventBus;
        this.placeController = placeController;
        this.masterActivitiesMapper = masterActivitiesMapper;
        this.detailsActivitiesMapper = detailsActivitiesMapper;
        this.detailsView = detailsView;
        this.editView = editView;
//        this.placeHistoryFactory = placeHistoryFactory;
    }

    @Override
    public void run() {
        /*
         * Add handlers, setup activities
         */
        init();

        /*
         * Hide the loading message
         */
        Element loading = Document.get().getElementById("loading");
        loading.getParentElement().removeChild(loading);

        /*
         * And show the user the shell
         */
        RootLayoutPanel.get().add(shell);
    }

    private void init() {
        GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
            @Override
            public void onUncaughtException(Throwable e) {
                Window.alert("Error: " + e.getMessage());
                log.log(Level.SEVERE, e.getMessage(), e);
            }
        });

        if (LogConfiguration.loggingIsEnabled()) {
            // Add remote logging handler
            RequestFactoryLogHandler.LoggingRequestProvider provider = new RequestFactoryLogHandler.LoggingRequestProvider() {
                @Override
                public LoggingRequest getLoggingRequest() {
                    return requestFactory.loggingRequest();
                }
            };
            Logger.getLogger("").addHandler(new RequestFactoryLogHandler(provider, Level.WARNING, new ArrayList<String>()));
        }

        RequestEvent.register(eventBus, new RequestEvent.Handler() {
            // Only show loading status if a request isn't serviced in 250ms.
            private static final int LOADING_TIMEOUT = 250;

            @Override
            public void onRequestEvent(RequestEvent requestEvent) {
                if (requestEvent.getState() == RequestEvent.State.SENT) {
                    shell.getMole().showDelayed(LOADING_TIMEOUT);
                } else {
                    shell.getMole().hide();
                }
            }
        });

        // details activity mapper
        final ActivityMapper detailsActivityMapper = new ActivityMapper() {
            @Override
            public Activity getActivity(Place place) {
                return place instanceof ActivityAwarePlace
                        ? ((ActivityAwarePlace) place).getActivity() : new EmptyActivity();
            }
        };
        
        // event bus handlers
        eventBus.addHandler(DetailsEvent.TYPE, new DetailsEventHandler<DocumentProxy>() {
            @Override
            public void onDetailsRequestReceivedForId(EntityProxyId<DocumentProxy> id) {
                placeController.goTo(new ProxyPlace(id, ProxyPlace.Operation.DETAILS,
                        new DocumentDetailsActivity(id, requestFactory, detailsView)));
            }
        });
        eventBus.addHandler(EditEvent.TYPE, new EditEventHandler<DocumentProxy>() {
            @Override
            public void onEditRequestReceivedForId(EntityProxyId<DocumentProxy> id) {
                editView.setCreating(false);
                Activity activity =
                        new FindAndEditProxyActivity<DocumentProxy>(id, requestFactory, editView) {
                    @Override
                    protected RequestContext createSaveRequestContextFor(DocumentProxy proxy) {
                        DocumentRequestContext request = requestFactory.documentRequestContext();
                        request.persist().using(proxy);
                        return request;
                    }
                };

                placeController.goTo(new ProxyPlace(id, ProxyPlace.Operation.EDIT, activity));
            }
        });
        eventBus.addHandler(DeleteEvent.TYPE, new DeleteEventHandler() {
            @Override
            public void onDeleteRequestReceived() {
                placeController.goTo(new ProxyListPlace(DocumentProxy.class));
            }
        });
        eventBus.addHandler(CreateEvent.TYPE, new CreateEventHandler() {
            @Override
            public void onCreateRequestReceivedForType(String typeName) {
                editView.setCreating(true);
                if (typeName.equals("Carte")) {
                    placeController.goTo(new ProxyPlace(DocumentProxy.class,
                            new BookActivity(requestFactory.documentRequestContext(), editView)));
                }
                if (typeName.equals("Revista")) {
                    placeController.goTo(new ProxyPlace(DocumentProxy.class,
                            new MagazineActivity(requestFactory.documentRequestContext(), editView)));
                }
                if (typeName.equals("Volum Conferinta")) {
                    placeController.goTo(new ProxyPlace(DocumentProxy.class,
                            new ConferenceVolumeActivity(requestFactory.documentRequestContext(), editView)));
                }
            }
        });

        // assembling

        ProxyPlaceToListPlace proxyPlaceToListPlace = new ProxyPlaceToListPlace();
        final ActivityManager masterActivityManager = new ActivityManager(masterActivitiesMapper, eventBus);
        masterActivityManager.setDisplay(shell.getMaster());

        final ActivityManager detailsActivityManager = new ActivityManager(detailsActivityMapper, eventBus);
        detailsActivityManager.setDisplay(shell.getDetails());

        ProxyListPlacePicker proxyListPlacePicker = new ProxyListPlacePicker(placeController, proxyPlaceToListPlace);
        HasConstrainedValue<Place> listPlacePickerView = shell.getPlacesBox();
        listPlacePickerView.setAcceptableValues(ImmutableSet.<Place>of(new ProxyListPlace(DocumentProxy.class),
                new ApplicationPlace(DocumentProxy.class)));
        proxyListPlacePicker.register(eventBus, listPlacePickerView);

        /*
         * Browser history integration
         */
//        ScaffoldPlaceHistoryMapper mapper = GWT.create(ScaffoldPlaceHistoryMapper.class);
//        mapper.setFactory(placeHistoryFactory);
//        PlaceHistoryHandler placeHistoryHandler = new PlaceHistoryHandler(mapper);
//        if (getTopPlaces().iterator().hasNext()) {
//            ProxyListPlace defaultPlace = getTopPlaces().iterator().next();
//            placeHistoryHandler.register(placeController, eventBus, defaultPlace);
//            placeHistoryHandler.handleCurrentHistory();
//        }
    }
}
