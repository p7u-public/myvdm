package com.myvdm.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.EntityProxyId;

/**
 *
 * @author razvan
 */
public interface EditEventHandler<T extends EntityProxy> extends EventHandler {
    
    void onEditRequestReceivedForId(EntityProxyId<T> id);
    
}
