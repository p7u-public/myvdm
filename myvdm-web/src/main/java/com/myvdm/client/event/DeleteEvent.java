package com.myvdm.client.event;

import com.google.web.bindery.event.shared.Event;

/**
 *
 * @author razvan
 */
public class DeleteEvent extends Event<DeleteEventHandler> {

    public static final Event.Type<DeleteEventHandler> TYPE = new Event.Type<DeleteEventHandler>();
    
    @Override
    public Type<DeleteEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(DeleteEventHandler handler) {
        handler.onDeleteRequestReceived();
    }
}
