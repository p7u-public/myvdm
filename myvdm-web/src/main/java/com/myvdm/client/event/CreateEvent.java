package com.myvdm.client.event;

import com.google.web.bindery.event.shared.Event;

/**
 *
 * @author razvan
 */
public class CreateEvent extends Event<CreateEventHandler> {

    public static final Event.Type<CreateEventHandler> TYPE = new Event.Type<CreateEventHandler>();

    private final String typeName;

    public CreateEvent(String typeName) {
        this.typeName = typeName;
    }
    
    @Override
    public Event.Type<CreateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CreateEventHandler handler) {
        handler.onCreateRequestReceivedForType(typeName);
    }
}
