package com.myvdm.client.event;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.EntityProxyId;

/**
 *
 * @author razvan
 */
public class DetailsEvent extends Event<DetailsEventHandler> {

    public static final Event.Type<DetailsEventHandler> TYPE = new Event.Type<DetailsEventHandler>();
    
    private final EntityProxyId<? extends EntityProxy> id;

    public DetailsEvent(EntityProxyId<? extends EntityProxy> id) {
        this.id = id;
    }

    @Override
    public Type<DetailsEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(DetailsEventHandler handler) {
        handler.onDetailsRequestReceivedForId(id);
    }
}
