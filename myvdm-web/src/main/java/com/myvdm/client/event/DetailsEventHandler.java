package com.myvdm.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.EntityProxyId;

/**
 *
 * @author razvan
 */
public interface DetailsEventHandler<T extends EntityProxy> extends EventHandler {
    
    void onDetailsRequestReceivedForId(EntityProxyId<T> id);
    
}
