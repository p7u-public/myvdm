package com.myvdm.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 *
 * @author razvan
 */
public interface CreateEventHandler extends EventHandler {
    
    void onCreateRequestReceivedForType(String typeName);
    
}
