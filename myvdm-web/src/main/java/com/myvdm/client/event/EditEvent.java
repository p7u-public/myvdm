package com.myvdm.client.event;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.EntityProxyId;

/**
 *
 * @author razvan
 */
public final class EditEvent extends Event<EditEventHandler> {

    public static final Event.Type<EditEventHandler> TYPE = new Event.Type<EditEventHandler>();

    private final EntityProxyId<? extends EntityProxy> id;
    
    public EditEvent(EntityProxyId<? extends EntityProxy> id) {
        this.id = id;
    }

    @Override
    public Type<EditEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EditEventHandler handler) {
        handler.onEditRequestReceivedForId(id);
    }
}
