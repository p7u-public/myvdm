package com.myvdm.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 *
 * @author razvan
 */
public interface DeleteEventHandler extends EventHandler {
    
    void onDeleteRequestReceived();
    
}
