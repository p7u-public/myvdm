package com.myvdm.client.ioc;

import com.google.gwt.inject.client.GinModules;
import com.myvdm.client.scaffold.ScaffoldDesktopApp;

/**
 *
 * @author razvan
 */
@GinModules(value = {ScaffoldModule.class})
public interface DesktopInjector extends ScaffoldInjector {

    @Override
    ScaffoldDesktopApp getScaffoldApp();
}