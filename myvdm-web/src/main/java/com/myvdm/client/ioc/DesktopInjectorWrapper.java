package com.myvdm.client.ioc;

import com.google.gwt.core.client.GWT;

/**
 *
 * @author razvan
 */
public class DesktopInjectorWrapper implements InjectorWrapper {

    @Override
    public ScaffoldInjector getInjector() {
        return GWT.create(DesktopInjector.class);
    }
}
