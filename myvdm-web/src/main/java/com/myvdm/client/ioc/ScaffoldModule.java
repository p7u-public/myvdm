package com.myvdm.client.ioc;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.myvdm.client.request.ApplicationRequestFactory;
import com.myvdm.client.request.DocumentProxy;
import com.myvdm.client.request.EventSourceRequestTransport;
import com.myvdm.client.ui.DocumentDetailsView;
import com.myvdm.client.ui.DocumentEditView;
import com.myvdm.client.ui.DocumentListView;
import com.myvdm.client.ui.ProxyDetailsView;
import com.myvdm.client.ui.ProxyEditView;
import com.myvdm.client.ui.ProxyListView;

/**
 *
 * @author razvan
 */
public class ScaffoldModule extends AbstractGinModule {

    @Override
    protected void configure() {
        bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);
        bind(ApplicationRequestFactory.class).toProvider(RequestFactoryProvider.class).in(Singleton.class);
        bind(PlaceController.class).toProvider(PlaceControllerProvider.class).in(Singleton.class);
        
        bind(new TypeLiteral<ProxyDetailsView<DocumentProxy>>(){}).to(DocumentDetailsView.class).in(Singleton.class);
        bind(new TypeLiteral<ProxyEditView<DocumentProxy, DocumentEditView>>(){}).to(DocumentEditView.class).in(Singleton.class);
        bind(new TypeLiteral<ProxyListView<DocumentProxy>>(){}).to(DocumentListView.class).in(Singleton.class);
    }

    static class PlaceControllerProvider implements Provider<PlaceController> {

        private final PlaceController placeController;

        @Inject
        public PlaceControllerProvider(EventBus eventBus) {
            this.placeController = new PlaceController(eventBus);
        }

        @Override
        public PlaceController get() {
            return placeController;
        }
    }

    static class RequestFactoryProvider implements Provider<ApplicationRequestFactory> {

        private final ApplicationRequestFactory requestFactory;

        @Inject
        public RequestFactoryProvider(EventBus eventBus) {
            requestFactory = GWT.create(ApplicationRequestFactory.class);
            requestFactory.initialize(eventBus, new EventSourceRequestTransport(eventBus));
        }

        @Override
        public ApplicationRequestFactory get() {
            return requestFactory;
        }
    }
}
