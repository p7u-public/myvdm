package com.myvdm.client.ioc;

import com.google.gwt.inject.client.Ginjector;
import com.myvdm.client.scaffold.ScaffoldApp;

/**
 *
 * @author razvan
 */
public interface ScaffoldInjector extends Ginjector {

	ScaffoldApp getScaffoldApp();
}
