package com.myvdm.client.ioc;

/**
 *
 * @author razvan
 */
public interface InjectorWrapper {

    ScaffoldInjector getInjector();
}
