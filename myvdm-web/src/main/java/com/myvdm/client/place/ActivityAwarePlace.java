package com.myvdm.client.place;

import com.google.gwt.activity.shared.Activity;

/**
 *
 * @author razvan
 */
public interface ActivityAwarePlace {
    
    public Activity getActivity();
    
}
