package com.myvdm.client.place;

import com.google.gwt.activity.shared.FilteredActivityMapper;
import com.google.gwt.place.shared.Place;

/**
 * 
 * Converts a {@link #ProxyPlace} to a {@link ProxyPlace}
 * 
 * @author razvan
 */
public class ProxyPlaceToListPlace implements FilteredActivityMapper.Filter {

    /**
     * Required by {@link FilteredActivityMapper.Filter}, calls
     * {@link #proxyPlaceFor()}.
     */
    @Override
    public Place filter(Place place) {
        return proxyPlaceFor(place);
    }

    /**
     * @param place a place to process
     * @return an appropriate ProxyPlace, or null if the given place has
     * nothing to do with proxies
     */
    public ProxyPlace proxyPlaceFor(Place place) {
        return place instanceof ProxyPlace ? (ProxyPlace) place : null;
    }
    
}
