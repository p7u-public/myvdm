package com.myvdm.client.place;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;
import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.EntityProxyId;
import com.google.web.bindery.requestfactory.shared.RequestFactory;

/**
 * A place in the app that deals with a specific {@link RequestFactory} proxy
 * 
 * @author razvan
 */
public class ProxyPlace extends Place implements ActivityAwarePlace {
    
    private final EntityProxyId<?> proxyId;
    
    private final Class<? extends EntityProxy> proxyClass;
    
    private final Operation operation;
    
    private final Activity activity;

    /**
     * The things you do with a record, each of which is a different
     * bookmarkable location in the scaffold app.
     */
    public enum Operation {

        CREATE, EDIT, DETAILS
    }

    /**
     * Tokenizer.
     */
    @Prefix("r")
    public static class Tokenizer implements PlaceTokenizer<ProxyPlace> {

        private static final String SEPARATOR = "!";
        private final RequestFactory requests;

        public Tokenizer(RequestFactory requests) {
            this.requests = requests;
        }

        @Override
        public ProxyPlace getPlace(String token) {
            String bits[] = token.split(SEPARATOR);
            Operation operation = Operation.valueOf(bits[1]);
            if (Operation.CREATE == operation) {
               // return new ProxyPlace(requests.getProxyClass(bits[0]));
            }
            //return new ProxyPlace(requests.getProxyId(bits[0]), operation);
            return null;
        }

        @Override
        public String getToken(ProxyPlace place) {
            if (Operation.CREATE == place.getOperation()) {
                return requests.getHistoryToken(place.getProxyClass()) + SEPARATOR + place.getOperation();
            }
            return requests.getHistoryToken(place.getProxyId()) + SEPARATOR + place.getOperation();
        }
    }

    @Override
    public Activity getActivity() {
        return activity;
    }

    public Operation getOperation() {
        return operation;
    }

    public Class<? extends EntityProxy> getProxyClass() {
        return proxyId != null ? proxyId.getProxyClass() : proxyClass;
    }

    /**
     * @return the proxyId, or null if the operation is {@link Operation#CREATE}
     */
    public EntityProxyId<? extends EntityProxy> getProxyId() {
        return proxyId;
    }

    /**
     * CREATE specific constructor
     */
    public ProxyPlace(Class<? extends EntityProxy> proxyClass, Activity activity) {
        this.operation = Operation.CREATE;
        this.proxyId = null;
        this.proxyClass = proxyClass;
        this.activity = activity;
    }

    public ProxyPlace(EntityProxyId<?> proxyId, Operation operation, Activity activity) {
        assert Operation.CREATE != operation;
        this.operation = operation;
        this.proxyId = proxyId;
        this.proxyClass = null;
        this.activity = activity;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ProxyPlace other = (ProxyPlace) obj;
        if (operation != other.operation) {
            return false;
        }
        if (proxyClass == null) {
            if (other.proxyClass != null) {
                return false;
            }
        } else if (!proxyClass.equals(other.proxyClass)) {
            return false;
        }
        if (proxyId == null) {
            if (other.proxyId != null) {
                return false;
            }
        } else if (!proxyId.equals(other.proxyId)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((operation == null) ? 0 : operation.hashCode());
        result = prime * result + ((proxyClass == null) ? 0 : proxyClass.hashCode());
        result = prime * result + ((proxyId == null) ? 0 : proxyId.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "ProxyPlace [operation=" + operation + ", proxy=" + proxyId + ", proxyClass=" + proxyClass + "]";
    }
}
