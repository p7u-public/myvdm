package com.myvdm.client.activity;

import com.google.gwt.activity.shared.AbstractActivity;
import com.myvdm.client.ui.ProxyDetailsView;

/**
 *
 * @author razvan
 */ 
public abstract class AbstractDetailsActivity extends AbstractActivity implements ProxyDetailsView.Delegate {
    
}
