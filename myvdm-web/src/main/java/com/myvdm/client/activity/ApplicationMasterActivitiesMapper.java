package com.myvdm.client.activity;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.myvdm.client.activity.uvt.ScopusIntegrationActivity;
import com.myvdm.client.place.ApplicationPlace;
import com.myvdm.client.request.ApplicationRequestFactory;
import com.myvdm.client.request.DocumentProxy;
import com.myvdm.client.ui.ProxyListView;
import com.myvdm.client.ui.uvt.ScopusIntegrationView;

/**
 *
 * @author razvan
 */
public final class ApplicationMasterActivitiesMapper implements ActivityMapper {

    private final Activity activity;
    
    @Inject
    public ApplicationMasterActivitiesMapper(ApplicationRequestFactory requests, PlaceController placeController, ProxyListView<DocumentProxy> listView, EventBus eventBus) {
        activity = new DocumentListActivity(requests, listView, eventBus);
    }
    
    @Override
    public Activity getActivity(Place place) {
        return place instanceof ApplicationPlace ? new ScopusIntegrationActivity(new ScopusIntegrationView()) : activity;
    }
}
