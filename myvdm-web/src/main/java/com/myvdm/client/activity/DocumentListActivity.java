package com.myvdm.client.activity;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.view.client.Range;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.Request;
import com.myvdm.client.event.CreateEvent;
import com.myvdm.client.request.ApplicationRequestFactory;
import com.myvdm.client.request.DocumentProxy;
import com.myvdm.client.request.DocumentTypeProxy;
import com.myvdm.client.ui.DocumentTypeProxyRenderer;
import com.myvdm.client.ui.ProxyListView;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author razvan
 */
public class DocumentListActivity extends AbstractProxyListActivity<DocumentProxy> {

    private final ApplicationRequestFactory requests;

    private final ProxyListView<DocumentProxy> listView;
    
    private DocumentTypeProxy typeProxy;
    
    private EventBus eventBus;

    public DocumentListActivity(ApplicationRequestFactory requests, ProxyListView<DocumentProxy> listView, EventBus eventBus) {
        super(listView, DocumentProxy.class);
        this.requests = requests;
        this.listView = listView;
    }

    @Override
    public void start(AcceptsOneWidget display, EventBus eventBus) {
        this.eventBus = eventBus;
        //view.setDocumentTypePickerValues(Collections.<DocumentTypeProxy>emptyList());
        requests.documentTypeRequestContext().findDocumentTypeEntries(0, 50)
                .with(DocumentTypeProxyRenderer.instance().getPaths())
                .fire(new Receiver<List<DocumentTypeProxy>>() {
            @Override
            public void onSuccess(List<DocumentTypeProxy> response) {
                List<DocumentTypeProxy> values = new ArrayList<DocumentTypeProxy>(response.size());
                values.addAll(response);
                listView.setDocumentTypePickerValues(values);
            }
        });
        super.start(display, eventBus);
    }

    @Override
    public void createClicked() {
        eventBus.fireEvent(new CreateEvent(typeProxy.getName()));
    }

    @Override
    public void typeChanged(DocumentTypeProxy proxy) {
        typeProxy = proxy;
    }

    @Override
    protected Request<List<DocumentProxy>> createRangeRequest(Range range) {
        return requests.documentRequestContext().findDocumentEntries(range.getStart(), range.getLength());
    }

    @Override
    protected void fireCountRequest(Receiver<Long> callback) {
        requests.documentRequestContext().countDocuments().fire(callback);
    }

    @Override
    public void onStop() {
        // Overrides because implementation from superclass creates NPE
    }
    
}
