package com.myvdm.client.activity;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.EntityProxyId;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.RequestFactory;
import com.myvdm.client.request.ApplicationRequestFactory;
import com.myvdm.client.ui.ProxyEditView;
import java.util.logging.Logger;

/**
 * Extends {@link AbstractProxyEditActivity} to work from a
 * {@link EntityProxyId}
 *
 * @param <P> the type of proxy to find and edit
 * 
 * @author razvan
 * 
 */
public abstract class FindAndEditProxyActivity<P extends EntityProxy> extends AbstractProxyEditActivity<P> {

    private final RequestFactory factory;
    private final EntityProxyId<P> proxyId;
    private P proxy;

    public FindAndEditProxyActivity(EntityProxyId<P> proxyId, RequestFactory factory, ProxyEditView<P, ?> view) {
        super(view);
        this.proxyId = proxyId;
        this.factory = factory;
    }

    @Override
    public void start(final AcceptsOneWidget display, final EventBus eventBus) {
        ((ApplicationRequestFactory) factory).documentRequestContext().find(proxyId).with("*.*").fire(new Receiver<P>() {
            @Override
            public void onSuccess(P response) {
                proxy = response;
                FindAndEditProxyActivity.super.start(display, eventBus);
            }
        });
    }

    @Override
    protected P getProxy() {
        return proxy;
    }
}
