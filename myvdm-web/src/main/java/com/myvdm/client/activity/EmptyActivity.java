package com.myvdm.client.activity;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author razvan
 */
public class EmptyActivity extends AbstractActivity {

    private static final Logger log = Logger.getLogger(EmptyActivity.class.getName());

    @Override
    public void start(AcceptsOneWidget panel, EventBus eventBus) {
        log.log(Level.WARNING, "Attempt to start empty activity !!!");
    }
}
