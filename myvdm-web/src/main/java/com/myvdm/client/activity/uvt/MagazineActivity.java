package com.myvdm.client.activity.uvt;

import com.google.common.collect.Lists;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.myvdm.client.activity.AbstractProxyEditActivity;
import com.myvdm.client.request.DocumentProxy;
import com.myvdm.client.request.DocumentRequestContext;
import com.myvdm.client.request.FieldProxy;
import com.myvdm.client.request.IntegerFieldValueProxy;
import com.myvdm.client.request.StringFieldValueProxy;
import com.myvdm.client.ui.ProxyEditView;

/**
 *
 * @author razvan
 */
public class MagazineActivity extends AbstractProxyEditActivity<DocumentProxy> {

    private DocumentRequestContext context;
    
    private final DocumentProxy proxy;

    public MagazineActivity(DocumentRequestContext context, ProxyEditView<DocumentProxy, ?> view) {
        super(view);
        
        // ~~ TEMP STUFF ~~
        proxy = context.create(DocumentProxy.class);
        
        // autori
        FieldProxy autori = context.create(FieldProxy.class);
        autori.setName("Autori");
        final StringFieldValueProxy av = context.create(StringFieldValueProxy.class);
        av.setValue("");
        autori.setValue(av);
        // titlu
        FieldProxy titlu = context.create(FieldProxy.class);
        titlu.setName("Titlu");
        final StringFieldValueProxy tv = context.create(StringFieldValueProxy.class);
        tv.setValue("");
        titlu.setValue(tv);
        // # pagini
        /* FieldProxy pagini = context.create(FieldProxy.class);
        pagini.setName("Nr. Pagini");
        final IntegerFieldValueProxy ifvp = context.create(IntegerFieldValueProxy.class);
        ifvp.setValue(0);
        pagini.setValue(ifvp); */
        // an
        FieldProxy an = context.create(FieldProxy.class);
        an.setName("An");
        final StringFieldValueProxy anv = context.create(StringFieldValueProxy.class);
        anv.setValue("");
        an.setValue(anv);
        // editura
        FieldProxy editura = context.create(FieldProxy.class);
        editura.setName("Editura");
        final StringFieldValueProxy edv = context.create(StringFieldValueProxy.class);
        edv.setValue("");
        editura.setValue(edv);
        // nume
        FieldProxy nume = context.create(FieldProxy.class);
        nume.setName("Nume");
        final StringFieldValueProxy nv = context.create(StringFieldValueProxy.class);
        nv.setValue("");
        nume.setValue(nv);
        // volum
        /*FieldProxy vol = context.create(FieldProxy.class);
        vol.setName("Volum");
        final IntegerFieldValueProxy vv = context.create(IntegerFieldValueProxy.class);
        vv.setValue(0);
        vol.setValue(vv);*/
         // adresa web
        FieldProxy web = context.create(FieldProxy.class);
        web.setName("Adresa web");
        final StringFieldValueProxy sfvp = context.create(StringFieldValueProxy.class);
        sfvp.setValue("");
        web.setValue(sfvp);
        // numar
        FieldProxy num = context.create(FieldProxy.class);
        num.setName("Numar");
        final IntegerFieldValueProxy nuv = context.create(IntegerFieldValueProxy.class);
        nuv.setValue(0);
        num.setValue(nv);
        
        proxy.setFields(Lists.newArrayList(autori,titlu,/*pagini,*/an,editura,nume,/*vol,*/web,num));
        // END
        
        this.context = context;
    }
    
    @Override
    protected RequestContext createSaveRequestContextFor(DocumentProxy proxy) {
        context.persist().using(proxy);
        return context;
    }

    @Override
    protected DocumentProxy getProxy() {
        return proxy;
    }
    
}
