package com.myvdm.client.activity;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.EntityProxyId;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.myvdm.client.request.DocumentProxy;
import com.myvdm.client.request.DocumentRequestContext;
import com.myvdm.client.place.ProxyPlace;
import com.myvdm.client.place.ActivityAwarePlace;
import com.myvdm.client.request.ApplicationRequestFactory;
import com.myvdm.client.ui.DocumentEditView;
import com.myvdm.client.ui.ProxyDetailsView;
import com.myvdm.client.ui.ProxyEditView;

/**
 *
 * @author razvan
 */
public class ApplicationDetailsActivitiesMapper implements ActivityMapper {

    private ApplicationRequestFactory requests;
    private ProxyDetailsView<DocumentProxy> detailsView;
    private ProxyEditView<DocumentProxy, DocumentEditView> editView;

    @Inject
    public ApplicationDetailsActivitiesMapper(ApplicationRequestFactory requestFactory,
            ProxyDetailsView<DocumentProxy> detailsView,
            ProxyEditView<DocumentProxy, DocumentEditView> editView) {
        this.requests = requestFactory;
        this.detailsView = detailsView;
        this.editView = editView;
    }

    @Override
    public Activity getActivity(Place place) {
        if (!(place instanceof ProxyPlace)) {
            return null;
        }
        final ProxyPlace proxyPlace = (ProxyPlace) place;

        switch (proxyPlace.getOperation()) {
            case DETAILS:
                return new DocumentDetailsActivity(coerceId(proxyPlace), requests, detailsView);
            case EDIT: {
                editView.setCreating(false);
                return makeEditActivity(proxyPlace);
            }
            case CREATE: {
                editView.setCreating(true);
                final ActivityAwarePlace myPlace = (ActivityAwarePlace) place;
                return myPlace.getActivity();

            }
            default:
                throw new IllegalArgumentException("Unknown operation " + proxyPlace.getOperation());
        }

    }

    private EntityProxyId<DocumentProxy> coerceId(ProxyPlace place) {
        return (EntityProxyId<DocumentProxy>) place.getProxyId();
    }

    private Activity makeEditActivity(ProxyPlace place) {
        EntityProxyId<DocumentProxy> proxyId = coerceId(place);
        Activity activity = new FindAndEditProxyActivity<DocumentProxy>(proxyId, requests, editView) {
            @Override
            protected RequestContext createSaveRequestContextFor(DocumentProxy proxy) {
                DocumentRequestContext request = requests.documentRequestContext();
                request.persist().using(proxy);
                return request;
            }
        };
        return activity;
    }
}
