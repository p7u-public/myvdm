package com.myvdm.client.activity;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.Range;
import com.google.web.bindery.requestfactory.shared.EntityProxyId;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.myvdm.client.event.CreateEvent;
import com.myvdm.client.event.DeleteEvent;
import com.myvdm.client.event.EditEvent;
import com.myvdm.client.request.ApplicationRequestFactory;
import com.myvdm.client.request.DocumentProxy;
import com.myvdm.client.request.FieldProxy;
import com.myvdm.client.ui.ProxyDetailsView;
import java.util.List;

/**
 *
 * @author razvan
 */
public class DocumentDetailsActivity extends AbstractDetailsActivity {

    private AcceptsOneWidget display;

    private final ProxyDetailsView<DocumentProxy> view;

    private final EntityProxyId<DocumentProxy> proxyId;

    private final ApplicationRequestFactory requests;
    
    private EventBus eventBus;

    public DocumentDetailsActivity(EntityProxyId<DocumentProxy> proxyId, ApplicationRequestFactory requests, ProxyDetailsView<DocumentProxy> view) {
        this.proxyId = proxyId;
        this.requests = requests;
        view.setDelegate(this);
        this.view = view;
    }

    @Override
    public void start(AcceptsOneWidget panel, final EventBus eventBus) {
        this.eventBus = eventBus;
        this.display = panel;

        Receiver<DocumentProxy> callback = new Receiver<DocumentProxy>() {
            @Override
            public void onSuccess(DocumentProxy response) {
                if (response == null) {
                    // TODO
                    eventBus.fireEvent(new CreateEvent(null));
                    return;
                }
                if (display == null) {
                    return;
                }
                final HasData<FieldProxy> hasData = ((ProxyDetailsView.SubProxyDetailView) view).subdetailAsHasData();
                List<FieldProxy> fields = response.getFields();
                ListDataProvider<FieldProxy> dataProvider = new ListDataProvider<FieldProxy>(fields);
                dataProvider.addDataDisplay(hasData);
                hasData.setVisibleRange(new Range(0, fields.size()));
                view.setValue(response);
                display.setWidget(view);
            }
        };
        find(callback);
    }

    protected void find(Receiver<DocumentProxy> callback) {
        // going as deep as fields.value
        requests.find(proxyId).with("type", "fields.value").fire(callback);
    }

    @Override
    public void deleteClicked() {
        // TODO i18n
        if (!view.confirm("Really delete this entry? You cannot undo this change.")) {
            return;
        }
        requests.documentRequestContext().remove().using(view.getValue()).fire(new Receiver<Void>() {
            @Override
            public void onSuccess(Void ignore) {
                if (display == null) {
                    return;
                }
                eventBus.fireEvent(new DeleteEvent());
            }
        });
    }

    @Override
    public void editClicked() {
        eventBus.fireEvent(new EditEvent(view.getValue().stableId()));
    }
}
