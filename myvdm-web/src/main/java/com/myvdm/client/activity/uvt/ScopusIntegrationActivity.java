package com.myvdm.client.activity.uvt;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;

/**
 *
 * @author razvan
 */
public class ScopusIntegrationActivity implements Activity {

    private final IsWidget view;
    
    public ScopusIntegrationActivity(IsWidget aView) {
        view = aView;
    }
    
    @Override
    public String mayStop() {
        return null;
    }

    @Override
    public void onCancel() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void start(AcceptsOneWidget panel, EventBus eventBus) {
        panel.setWidget(view);
    }
    
}
