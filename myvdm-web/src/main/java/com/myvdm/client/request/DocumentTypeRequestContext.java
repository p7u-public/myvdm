package com.myvdm.client.request;

import com.google.web.bindery.requestfactory.shared.InstanceRequest;
import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.Service;
import com.myvdm.server.domain.DocumentType;
import java.util.List;

/**
 *
 * @author Razvan Petruescu
 */
@Service(DocumentType.class)
public interface DocumentTypeRequestContext extends RequestContext {

    InstanceRequest<DocumentTypeProxy, Void> persist();
    
    InstanceRequest<DocumentTypeProxy, Void> remove();

    abstract Request<Long> countDocumentTypes();
    
    abstract Request<List<DocumentTypeProxy>> findDocumentTypeEntries(int firstResult, int maxResults);
   
    
}
