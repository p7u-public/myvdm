package com.myvdm.client.request;

import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.myvdm.server.domain.IntegerFieldValue;

/**
 *
 * @author razvan
 */
@ProxyFor(IntegerFieldValue.class)
public interface IntegerFieldValueProxy extends FieldValueProxy, EntityProxy {
    
    public Integer getValue();

    public void setValue(Integer value);
    
}
