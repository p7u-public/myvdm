package com.myvdm.client.request;

import com.google.web.bindery.requestfactory.shared.*;
import com.myvdm.server.domain.Document;
import java.util.List;

/**
 *
 * @author razvan
 */
@Service(Document.class)
@ExtraTypes({IntegerFieldValueProxy.class, StringFieldValueProxy.class})
public interface DocumentRequestContext extends RequestContext {
    
    InstanceRequest<DocumentProxy, Void> persist();
    
    InstanceRequest<DocumentProxy, Void> remove();

    abstract Request<Long> countDocuments();
    
    abstract Request<List<DocumentProxy>> findDocumentEntries(int firstResult, int maxResults);
    
}
