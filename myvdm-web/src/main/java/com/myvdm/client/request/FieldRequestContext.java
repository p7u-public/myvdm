package com.myvdm.client.request;

import com.google.web.bindery.requestfactory.shared.ExtraTypes;
import com.google.web.bindery.requestfactory.shared.InstanceRequest;
import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.Service;
import com.myvdm.server.domain.Field;
import java.util.List;

/**
 *
 * @author razvan
 */
@Service(Field.class)
@ExtraTypes({IntegerFieldValueProxy.class, StringFieldValueProxy.class})
public interface FieldRequestContext extends RequestContext {
    
    InstanceRequest<FieldProxy, Void> persist();
    
    InstanceRequest<FieldProxy, Void> remove();

    abstract Request<Long> countFields();
    
    abstract Request<List<FieldProxy>> findFieldEntries(int firstResult, int maxResults);
}
