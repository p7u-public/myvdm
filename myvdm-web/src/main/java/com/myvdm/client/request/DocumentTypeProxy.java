package com.myvdm.client.request;

import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.myvdm.server.domain.DocumentType;

/**
 *
 * @author razvan
 */
@ProxyFor(DocumentType.class)
public interface DocumentTypeProxy extends EntityProxy {
    
    String getName();
    
    void setName(String name);
    
}
