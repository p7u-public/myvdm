package com.myvdm.client.request;

import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.myvdm.server.domain.Field;

/**
 *
 * @author razvan
 */
@ProxyFor(Field.class)
public interface FieldProxy extends EntityProxy {
    
    String getName();
    
    void setName(String name);
    
    FieldValueProxy getValue();
    
    void setValue(FieldValueProxy fieldValueProxy);
    
}
