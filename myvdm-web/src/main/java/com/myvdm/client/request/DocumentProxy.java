package com.myvdm.client.request;

import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.myvdm.server.domain.Document;
import java.util.List;

/**
 *
 * @author razvan
 */
@ProxyFor(Document.class)
public interface DocumentProxy extends EntityProxy {
    
    String getName();
    
    void setName(String name);
    
    DocumentTypeProxy getType();
    
    void setType(DocumentTypeProxy type);
    
    public List<FieldProxy> getFields();
    
    void setFields(List<FieldProxy> fields);
    
}
