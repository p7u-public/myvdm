package com.myvdm.client.request;

import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.myvdm.server.domain.FieldValue;

/**
 *
 * @author razvan
 */
@ProxyFor(FieldValue.class)
public interface FieldValueProxy extends EntityProxy {
}
