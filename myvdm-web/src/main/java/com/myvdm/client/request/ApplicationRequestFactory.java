package com.myvdm.client.request;

/**
 *
 * @author razvan
 */
public interface ApplicationRequestFactory extends ScaffoldRequestFactory {

    DocumentRequestContext documentRequestContext();
    
    DocumentTypeRequestContext documentTypeRequestContext();
    
    FieldRequestContext fieldRequestContext();
    
}
