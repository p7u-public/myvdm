package com.myvdm.client.request;

import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.myvdm.server.domain.StringFieldValue;

/**
 *
 * @author razvan
 */
@ProxyFor(StringFieldValue.class)
public interface StringFieldValueProxy extends FieldValueProxy, EntityProxy {
   
    public String getValue();

    public void setValue(String value);
    
}
