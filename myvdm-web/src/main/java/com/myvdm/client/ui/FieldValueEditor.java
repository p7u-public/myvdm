package com.myvdm.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.LazyPanel;
import com.google.gwt.user.client.ui.Widget;
import com.myvdm.client.request.FieldValueProxy;
import com.myvdm.client.request.IntegerFieldValueProxy;
import com.myvdm.client.request.StringFieldValueProxy;

/**
 *
 * @author Razvan Petruescu
 */
public class FieldValueEditor extends Composite implements Editor<FieldValueProxy> {

    interface Binder extends UiBinder<Widget, FieldValueEditor> {
    }

    @UiField(provided = true)
    @Ignore
    final StringFieldValueEditor stringFieldValueEditor = new StringFieldValueEditor();

    @UiField
    LazyPanel stringFieldValuePanel;

    @UiField(provided = true)
    @Ignore
    final IntegerFieldValueEditor integerFieldValueEditor = new IntegerFieldValueEditor();

    @UiField
    LazyPanel integerFieldValuePanel;

    public FieldValueEditor() {
        super.initWidget(GWT.<Binder>create(Binder.class).createAndBindUi(this));
    }

    @Path("")
    final AbstractSubTypeEditor<FieldValueProxy, StringFieldValueProxy, StringFieldValueEditor> stringSubTypeEditor =
            new AbstractSubTypeEditor<FieldValueProxy, StringFieldValueProxy, StringFieldValueEditor>(stringFieldValueEditor) {
                @Override
                public void setValue(FieldValueProxy value) {
                    final boolean isInstanceOf = value instanceof StringFieldValueProxy;
                    setValue(value, isInstanceOf);
                    if (isInstanceOf) {
                        stringFieldValuePanel.setVisible(true);
                    }
                }
            };

    @Path("")
    final AbstractSubTypeEditor<FieldValueProxy, IntegerFieldValueProxy, IntegerFieldValueEditor> integerSubTypeEditor =
            new AbstractSubTypeEditor<FieldValueProxy, IntegerFieldValueProxy, IntegerFieldValueEditor>(integerFieldValueEditor) {
                @Override
                public void setValue(FieldValueProxy value) {
                    final boolean isInstanceOf = value instanceof IntegerFieldValueProxy;
                    setValue(value, isInstanceOf);
                    if (isInstanceOf) {
                        integerFieldValuePanel.setVisible(true);
                    }
                }
            };

}
