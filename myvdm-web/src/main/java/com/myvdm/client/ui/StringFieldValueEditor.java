package com.myvdm.client.ui;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.myvdm.client.request.StringFieldValueProxy;

/**
 *
 * @author Razvan Petruescu
 */
public class StringFieldValueEditor extends Composite implements Editor<StringFieldValueProxy> {
    
    TextBox value = new TextBox();

    public StringFieldValueEditor() {
        super.initWidget(value);
    }

}
