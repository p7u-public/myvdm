package com.myvdm.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.IsEditor;
import com.google.gwt.editor.client.adapters.EditorSource;
import com.google.gwt.editor.client.adapters.ListEditor;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.requestfactory.gwt.client.RequestFactoryEditorDriver;
import com.myvdm.client.request.FieldProxy;
import java.util.List;

/**
 * @author Razvan Petruescu
 */
public class FieldListEditor extends Composite implements IsEditor<ListEditor<FieldProxy, FieldEditor>> {

    private final ListEditor<FieldProxy, FieldEditor> editor;

    @UiField
    FlexTable table;

    @UiField
    FlowPanel editorPanel;

    interface Binder extends UiBinder<Widget, FieldListEditor> {
    }

    interface FieldListEditorDriver extends RequestFactoryEditorDriver<List<FieldProxy>, ListEditor<FieldProxy, FieldEditor>> {
    }

    interface Style extends CssResource {

        String editorPanelHidden();

        String editorPanelVisible();

        String viewPanelHidden();

        String viewPanelVisible();
    }

    public FieldListEditor() {
        super.initWidget(GWT.<Binder>create(Binder.class).createAndBindUi(this));
        final FieldListEditorDriver driver = GWT.create(FieldListEditorDriver.class);
        editor = ListEditor.of(new FieldEditorSource());
        driver.initialize(editor);
    }

    @Override
    public ListEditor<FieldProxy, FieldEditor> asEditor() {
        return editor;
    }

    class FieldEditorSource extends EditorSource<FieldEditor> {

        @Override
        public FieldEditor create(int index) {
            FieldEditor editor = new FieldEditor();
            addRow(index, editor);
            return editor;
        }

        @Override
        public void dispose(FieldEditor subEditor) {
            removeRow(subEditor);
        }
    }

    private void addRow(int rowIndex, Widget widget) {
        table.setWidget(rowIndex, 0, widget);
    }

    private void removeRow(Widget row) {
        table.remove(row);
    }
}
