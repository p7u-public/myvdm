package com.myvdm.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.myvdm.client.request.FieldProxy;
import com.myvdm.client.request.IntegerFieldValueProxy;
import com.myvdm.client.request.StringFieldValueProxy;

/**
 * @author Razvan Petruescu
 */
public class DocumentFieldsView extends Composite {

    interface DocumentFieldsViewUiBinder extends UiBinder<Widget, DocumentFieldsView> {
    }
    
    private static DocumentFieldsViewUiBinder uiBinder = GWT.create(DocumentFieldsViewUiBinder.class);

    @UiField
    CellTable<FieldProxy> fields;

    public DocumentFieldsView() {
        initWidget(uiBinder.createAndBindUi(this));
        
        TextColumn<FieldProxy> nameColumn = new TextColumn<FieldProxy>() {
            @Override
            public String getValue(FieldProxy object) {
                return object.getName();
            }
        };
        fields.addColumn(nameColumn);

        TextColumn<FieldProxy> valueColumn = new TextColumn<FieldProxy>() {
            @Override
            public String getValue(FieldProxy proxy) {
                if (proxy.getValue() instanceof StringFieldValueProxy) {
                    return ((StringFieldValueProxy) proxy.getValue()).getValue();
                }
                if (proxy.getValue() instanceof IntegerFieldValueProxy) {
                    return ((IntegerFieldValueProxy) proxy.getValue()).getValue().toString();
                }
                return "OOPS!";
            }
        };
        fields.addColumn(valueColumn);
    }
    
}
