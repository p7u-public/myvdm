package com.myvdm.client.ui;

import com.google.web.bindery.requestfactory.gwt.ui.client.ProxyRenderer;
import com.myvdm.client.request.DocumentTypeProxy;

/**
 *
 * @author razvan
 */
public class DocumentTypeProxyRenderer extends ProxyRenderer<DocumentTypeProxy> {

    private static DocumentTypeProxyRenderer INSTANCE;

    protected DocumentTypeProxyRenderer() {
        super(new String[] { "name" });
    }

    public static DocumentTypeProxyRenderer instance() {
        if (INSTANCE == null) {
            INSTANCE = new DocumentTypeProxyRenderer();
        }
        return INSTANCE;
    }

    @Override
    public String render(DocumentTypeProxy proxy) {
        if (proxy == null) {
            return "";
        }
        return proxy.getName();
    }
}
