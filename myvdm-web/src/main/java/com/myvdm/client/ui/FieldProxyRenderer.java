package com.myvdm.client.ui;

import com.google.web.bindery.requestfactory.gwt.ui.client.ProxyRenderer;
import com.myvdm.client.request.FieldProxy;

/**
 *
 * @author razvan
 */
public final class FieldProxyRenderer extends ProxyRenderer<FieldProxy> {

    private static FieldProxyRenderer INSTANCE;

    private FieldProxyRenderer() {
        super(new String[]{"name", "value"});
    }

    public static FieldProxyRenderer instance() {
        if (INSTANCE == null) {
            INSTANCE = new FieldProxyRenderer();
        }
        return INSTANCE;
    }

    @Override
    public String render(FieldProxy proxy) {
        // TODO null check
        if (proxy == null) {
            return "NULL";
        }
        return proxy.getName();
    }
}
