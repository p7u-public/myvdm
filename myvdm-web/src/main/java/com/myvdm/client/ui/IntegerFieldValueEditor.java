package com.myvdm.client.ui;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IntegerBox;
import com.myvdm.client.request.IntegerFieldValueProxy;

/**
 *
 * @author razvan
 */
public class IntegerFieldValueEditor extends Composite implements Editor<IntegerFieldValueProxy>{
    
    IntegerBox value = new IntegerBox();

    public IntegerFieldValueEditor() {
        super.initWidget(value);
    }
    
}
