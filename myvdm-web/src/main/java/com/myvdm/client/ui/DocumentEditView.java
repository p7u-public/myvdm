package com.myvdm.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.editor.client.EditorError;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import com.google.web.bindery.requestfactory.gwt.client.RequestFactoryEditorDriver;
import com.myvdm.client.request.DocumentProxy;
import java.util.List;

/**
 *
 * @author razvan
 */
public class DocumentEditView extends Composite implements ProxyEditView<DocumentProxy, DocumentEditView> {

    interface Binder extends UiBinder<HTMLPanel, DocumentEditView> {
    }

    interface Driver extends RequestFactoryEditorDriver<DocumentProxy, DocumentEditView> {
    }
    private static final Binder BINDER = GWT.create(Binder.class);
    
    private Delegate delegate;
    
    @UiField
    TextBox name;
    
    @UiField
    FieldListEditor fields;
    
    @UiField
    FlowPanel fieldsPanel;
    
    @UiField
    Button cancel;
    
    @UiField
    Button save;
    
    @UiField
    DivElement errors;
    
    @UiField
    Element editTitle;
    
    @UiField
    Element createTitle;

    public DocumentEditView() {
        super.initWidget(BINDER.createAndBindUi(this));
    }

    @Override
    public RequestFactoryEditorDriver<DocumentProxy, DocumentEditView> createEditorDriver() {
        RequestFactoryEditorDriver<DocumentProxy, DocumentEditView> driver = GWT.create(Driver.class);
        driver.initialize(this);
        return driver;
    }

    @Override
    public void setDelegate(Delegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public void setEnabled(boolean enabled) {
        save.setEnabled(enabled);
    }

    @Override
    public void showErrors(List<EditorError> errors) {
        SafeHtmlBuilder b = new SafeHtmlBuilder();
        for (EditorError error : errors) {
            b.appendEscaped(error.getPath()).appendEscaped(": ");
            b.appendEscaped(error.getMessage()).appendHtmlConstant("<br>");
        }
        this.errors.setInnerHTML(b.toSafeHtml().asString());
    }

    // TODO refactor to 2 methods, as a method with one boolean parameter does 
    // 2 things
    @Override
    public void setCreating(boolean creating) {
        if (creating) {
            editTitle.getStyle().setDisplay(Display.NONE);
            createTitle.getStyle().clearDisplay();
        } else {
            editTitle.getStyle().clearDisplay();
            createTitle.getStyle().setDisplay(Display.NONE);
        }
    }

    @UiHandler("cancel")
    void onCancel(ClickEvent event) {
        delegate.cancelClicked();
    }

    @UiHandler("save")
    void onSave(ClickEvent event) {
        delegate.saveClicked();
    }
}
