package com.myvdm.client.ui;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.web.bindery.requestfactory.shared.EntityProxy;

/**
 *
 * @author Razvan Petruescu
 */
public abstract class AbstractProxyDetailsView<P extends EntityProxy> extends Composite implements ProxyDetailsView<P> {

    protected void init(Widget root, HasData<P> display) {
        super.initWidget(root);
    }

    @Override
    public Widget asWidget() {
        return this;
    }
    
    @Override
    public boolean confirm(String msg) {
        return Window.confirm(msg);
    }

}
