package com.myvdm.client.ui;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.Composite;
import com.myvdm.client.request.FieldValueProxy;

/**
 *
 * @author razvan
 */
public class FieldList extends Composite implements Editor<FieldValueProxy> {
    
    StringFieldValueEditor stringFieldValueEditor = new StringFieldValueEditor();

    public FieldList() {
        super.initWidget(stringFieldValueEditor);
    }
    
}
