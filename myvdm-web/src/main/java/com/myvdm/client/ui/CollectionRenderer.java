package com.myvdm.client.ui;

import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.text.shared.Renderer;

import java.util.Collection;

/**
 * A renderer for Collections that is parameterized by another renderer
 * 
 * @author razvan
 */
public class CollectionRenderer<E, R extends Renderer<E>, T extends Collection<E>> extends AbstractRenderer<T> implements Renderer<T> {

	public static <E, R extends Renderer<E>, T extends Collection<E>> CollectionRenderer<E, R, T> of(R r) {
		return new CollectionRenderer<E, R, T>(r);
	}

	private R elementRenderer;

	public CollectionRenderer(R elementRenderer) {
		this.elementRenderer = elementRenderer;
	}

	@Override
	public String render(T t) {
		StringBuilder toReturn = new StringBuilder(/*t.size()*/);
		boolean first = true;
                // TODO null check
		if (t != null) {
			for (E e : t) {
				if (!first) {
					toReturn.append(',');
				} else {
					first = false;
				}
				toReturn.append(elementRenderer.render(e));
			}
		}
		return toReturn.toString();
	}
}
