package com.myvdm.client.ui.uvt;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.Messages;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author razvan
 */
public class ScopusIntegrationView extends Composite {

    interface ScopusIntegrationViewUiBinder extends UiBinder<Widget, ScopusIntegrationView> {
    }

    private static ScopusIntegrationViewUiBinder uiBinder = GWT.create(ScopusIntegrationViewUiBinder.class);

    @UiField
    TextBox byNameSearchBox;

    @UiField
    TextBox byReferenceSearchBox;

    @UiField
    HasClickHandlers searchByName;

    @UiField
    HasClickHandlers searchByReference;

    private Interpolator interpolator = GWT.create(Interpolator.class);

    interface Interpolator extends Messages {

        @DefaultMessage("AUTH({0})")
        String authorField(String author);

        @DefaultMessage("SUBJAREA(COMP)")
        String subjectAreaField();

        @DefaultMessage("AFFIL(Timisoara and west)")
        String affiliationField();

        @DefaultMessage("REF({0})")
        String generalReferenceField(String param1);

        @DefaultMessage("REFAUTH({0})")
        String referenceAuthorField(String author);

    }

    public ScopusIntegrationView() {
        initWidget(uiBinder.createAndBindUi(this));
        ScriptInjector.fromUrl("http://api.elsevier.com/javascript/scopussearch.jsp").setCallback(new Callback<Void, Exception>() {
            @Override
            public void onFailure(Exception reason) {
                throw new UnsupportedOperationException("FAILURE to inject Scopus API !!!");
            }

            @Override
            public void onSuccess(Void result) {
                // TODO replace with LOG:
                System.out.println("Elsevier scopus search API successfully injected...");
            }
        }).setWindow(ScriptInjector.TOP_WINDOW).inject();
    }

    @Override
    public Widget asWidget() {
        return this;
    }

    @UiHandler("searchByName")
    public void byNameSearchBoxClicked(ClickEvent e) {
        final StringBuilder searchQ = new StringBuilder(interpolator.authorField(byNameSearchBox.getValue()));
        searchQ.append(" ");
        searchQ.append(interpolator.affiliationField());
        searchQ.append(" ");
        searchQ.append(interpolator.subjectAreaField());
        System.out.println(searchQ);
        performSearch(searchQ.toString());
    }

    @UiHandler("searchByReference")
    public void byReferenceSearchBoxClicked(ClickEvent e) {
        final String referenceAuthorField = interpolator.referenceAuthorField(byReferenceSearchBox.getValue());
        final String generalReferenceField = interpolator.generalReferenceField(referenceAuthorField);
        
        final StringBuilder searchQ = new StringBuilder(generalReferenceField);
        searchQ.append(" ");
        searchQ.append(interpolator.affiliationField());
        searchQ.append(" ");
        searchQ.append(interpolator.subjectAreaField());
        System.out.println(searchQ);
        performSearch(searchQ.toString());
    }

    private native void performSearch(String searchQuery) /*-{
     
     $wnd.sciverse.setApiKey("3f05a66b82f4e2ba987fd0f36014da42");
     
     var search = new $wnd.searchObj();
     search.setSearch(searchQuery);
     
     $wnd.sciverse.search(search); 
      
     }-*/;

}
