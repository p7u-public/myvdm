package com.myvdm.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.myvdm.client.request.DocumentProxy;
import com.myvdm.client.request.FieldProxy;
import com.myvdm.client.ui.ProxyDetailsView.SubProxyDetailView;

/**
 *
 * @author razvan
 */
public class DocumentDetailsView extends Composite implements ProxyDetailsView<DocumentProxy>, SubProxyDetailView<FieldProxy> {

    interface DocumentDetailsViewUiBinder extends UiBinder<Widget, DocumentDetailsView> {
    }

    private static DocumentDetailsViewUiBinder uiBinder = GWT.create(DocumentDetailsViewUiBinder.class);

    @UiField
    SpanElement displayRenderer;

    @UiField
    SpanElement name;

    @UiField
    DocumentFieldsView fieldsView;

    @UiField
    HasClickHandlers edit;

    @UiField
    HasClickHandlers delete;

    private Delegate delegate;

    private DocumentProxy proxy;

    public DocumentDetailsView() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    @Override
    public boolean confirm(String msg) {
        return Window.confirm(msg);
    }

    @Override
    public void setDelegate(Delegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public Widget asWidget() {
        return this;
    }

    @UiHandler("delete")
    public void onDeleteClicked(ClickEvent e) {
        delegate.deleteClicked();
    }

    @UiHandler("edit")
    public void onEditClicked(ClickEvent e) {
        delegate.editClicked();
    }

    @Override
    public HasData<FieldProxy> subdetailAsHasData() {
        return fieldsView.fields;
    }

    @Override
    public void setValue(DocumentProxy proxy) {
        this.proxy = proxy;
    }

    @Override
    public DocumentProxy getValue() {
        return proxy;
    }
}
