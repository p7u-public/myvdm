package com.myvdm.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.myvdm.client.request.FieldProxy;

/**
 *
 * @author Razvan Petruescu
 */
public class FieldEditor extends Composite implements Editor<FieldProxy> {

    interface Binder extends UiBinder<Widget, FieldEditor> {
    }

    @UiField
    Label name;

    @UiField
    FieldValueEditor value = new FieldValueEditor();
    
    public FieldEditor() {
        super.initWidget(GWT.<Binder>create(Binder.class).createAndBindUi(this));
    }

}
