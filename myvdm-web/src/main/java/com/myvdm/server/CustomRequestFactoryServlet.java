package com.myvdm.server;

import com.google.web.bindery.requestfactory.server.ExceptionHandler;
import com.google.web.bindery.requestfactory.server.RequestFactoryServlet;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author razvan
 */
public class CustomRequestFactoryServlet extends RequestFactoryServlet {

    private static class ApplicationExceptionLogger implements ExceptionHandler {

        private final Logger log = LoggerFactory.getLogger(ApplicationExceptionLogger.class);

        @Override
        public ServerFailure createServerFailure(Throwable throwable) {
            log.error("Server Error", throwable);
            return new ServerFailure(throwable.getMessage(), throwable.getClass().getName(), throwable.getStackTrace().toString(), true);
        }
    }

    public CustomRequestFactoryServlet() {
        super(new ApplicationExceptionLogger());
    }
}
