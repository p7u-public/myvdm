package com.myvdm.server.domain;

import javax.persistence.DiscriminatorValue;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

/**
 *
 * @author razvan
 */ 
@RooJavaBean
@RooToString
@RooJpaActiveRecord
@DiscriminatorValue(value="integer")
public class IntegerFieldValue extends FieldValue implements ValueHolder<Integer> {
    
    private Integer value;

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }

}
