package com.myvdm.server.domain;

import javax.persistence.DiscriminatorValue;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

/**
 *
 * @author razvan
 */ 
@RooJavaBean
@RooToString
@RooJpaActiveRecord
@DiscriminatorValue(value="string")
public class StringFieldValue extends FieldValue implements ValueHolder<String> {

    private String value;
    
    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }
    
}
