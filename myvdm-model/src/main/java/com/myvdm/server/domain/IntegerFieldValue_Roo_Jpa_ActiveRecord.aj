// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.myvdm.server.domain;

import com.myvdm.server.domain.IntegerFieldValue;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

privileged aspect IntegerFieldValue_Roo_Jpa_ActiveRecord {
    
    public static long IntegerFieldValue.countIntegerFieldValues() {
        return entityManager().createQuery("SELECT COUNT(o) FROM IntegerFieldValue o", Long.class).getSingleResult();
    }
    
    public static List<IntegerFieldValue> IntegerFieldValue.findAllIntegerFieldValues() {
        return entityManager().createQuery("SELECT o FROM IntegerFieldValue o", IntegerFieldValue.class).getResultList();
    }
    
    public static IntegerFieldValue IntegerFieldValue.findIntegerFieldValue(Long id) {
        if (id == null) return null;
        return entityManager().find(IntegerFieldValue.class, id);
    }
    
    public static List<IntegerFieldValue> IntegerFieldValue.findIntegerFieldValueEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM IntegerFieldValue o", IntegerFieldValue.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public IntegerFieldValue IntegerFieldValue.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        IntegerFieldValue merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
