package com.myvdm.server.domain;


/**
 *
 * @author razvan
 */ 
public interface ValueHolder<T> {
    
    abstract T getValue();

    abstract void setValue(T value);
    
}
