package com.myvdm.server.domain;

import javax.persistence.CascadeType;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

/**
 *
 * @author razvan
 */ 
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Field {

    @Size(min = 1, max = 64)
    private String name;

    // TODO enable this and the web IT will fail !
    //@NotNull
    // TODO Add cascade-remove and the model IT will fail !
    @OneToOne(cascade = {CascadeType.PERSIST, 
                            CascadeType.DETACH})
    private FieldValue value;

}
