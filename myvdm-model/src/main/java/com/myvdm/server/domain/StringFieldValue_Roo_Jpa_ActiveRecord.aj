// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.myvdm.server.domain;

import com.myvdm.server.domain.StringFieldValue;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

privileged aspect StringFieldValue_Roo_Jpa_ActiveRecord {
    
    public static long StringFieldValue.countStringFieldValues() {
        return entityManager().createQuery("SELECT COUNT(o) FROM StringFieldValue o", Long.class).getSingleResult();
    }
    
    public static List<StringFieldValue> StringFieldValue.findAllStringFieldValues() {
        return entityManager().createQuery("SELECT o FROM StringFieldValue o", StringFieldValue.class).getResultList();
    }
    
    public static StringFieldValue StringFieldValue.findStringFieldValue(Long id) {
        if (id == null) return null;
        return entityManager().find(StringFieldValue.class, id);
    }
    
    public static List<StringFieldValue> StringFieldValue.findStringFieldValueEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM StringFieldValue o", StringFieldValue.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public StringFieldValue StringFieldValue.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        StringFieldValue merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
