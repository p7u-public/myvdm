package com.myvdm.server.domain;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

/**
 *
 * @author razvan
 */ 
@RooJavaBean
@RooToString
@RooJpaActiveRecord
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name="FV")
public abstract class FieldValue {
}
