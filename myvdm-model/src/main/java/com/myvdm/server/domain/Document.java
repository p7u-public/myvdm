package com.myvdm.server.domain;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

/**
 *
 * @author razvan
 */ 
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Document {

    @NotNull
    private String name;

    @ManyToOne
    private DocumentType type;

    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
                                                          CascadeType.REMOVE, CascadeType.DETACH } )
    private List<Field> fields;
}
