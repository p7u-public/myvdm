// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.myvdm.server.domain;

import com.myvdm.server.domain.FieldValue;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect FieldValue_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager FieldValue.entityManager;
    
    public static final EntityManager FieldValue.entityManager() {
        EntityManager em = new FieldValue() {
        }.entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long FieldValue.countFieldValues() {
        return entityManager().createQuery("SELECT COUNT(o) FROM FieldValue o", Long.class).getSingleResult();
    }
    
    public static List<FieldValue> FieldValue.findAllFieldValues() {
        return entityManager().createQuery("SELECT o FROM FieldValue o", FieldValue.class).getResultList();
    }
    
    public static FieldValue FieldValue.findFieldValue(Long id) {
        if (id == null) return null;
        return entityManager().find(FieldValue.class, id);
    }
    
    public static List<FieldValue> FieldValue.findFieldValueEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM FieldValue o", FieldValue.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void FieldValue.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void FieldValue.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            FieldValue attached = FieldValue.findFieldValue(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void FieldValue.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void FieldValue.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public FieldValue FieldValue.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        FieldValue merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
