------------------------------------------------------------------
-- DataNucleus SchemaTool (ran at 16/01/2013 19:18:29)
------------------------------------------------------------------
-- Complete schema required for the following classes:-
--     com.myvdm.server.domain.Document
--     com.myvdm.server.domain.DocumentType
--     com.myvdm.server.domain.Field
--     com.myvdm.server.domain.FieldValue
--     com.myvdm.server.domain.IntegerFieldValue
--     com.myvdm.server.domain.StringFieldValue
--
-- Table "INTEGERFIELDVALUE" for classes [com.myvdm.server.domain.IntegerFieldValue]
CREATE TABLE "INTEGERFIELDVALUE"
(
    "ID" int8 NOT NULL,
    "VALUE" int4 NULL,
    CONSTRAINT "INTEGERFIELDVALUE_PK" PRIMARY KEY ("ID")
);

-- Table "FIELD" for classes [com.myvdm.server.domain.Field]
CREATE TABLE "FIELD"
(
    "ID" SERIAL,
    "NAME" varchar(255) NULL,
    "VALUE_ID" int8 NULL,
    "VERSION" int4 NULL,
    CONSTRAINT "FIELD_PK" PRIMARY KEY ("ID")
);

-- Table "DOCUMENTTYPE" for classes [com.myvdm.server.domain.DocumentType]
CREATE TABLE "DOCUMENTTYPE"
(
    "ID" SERIAL,
    "NAME" varchar(255) NULL,
    "VERSION" int4 NULL,
    CONSTRAINT "DOCUMENTTYPE_PK" PRIMARY KEY ("ID")
);

-- Table "STRINGFIELDVALUE" for classes [com.myvdm.server.domain.StringFieldValue]
CREATE TABLE "STRINGFIELDVALUE"
(
    "ID" int8 NOT NULL,
    "VALUE" varchar(255) NULL,
    CONSTRAINT "STRINGFIELDVALUE_PK" PRIMARY KEY ("ID")
);

-- Table "FIELDVALUE" for classes [com.myvdm.server.domain.FieldValue]
CREATE TABLE "FIELDVALUE"
(
    "ID" SERIAL,
    "VERSION" int4 NULL,
    "FV" varchar(31) NOT NULL,
    CONSTRAINT "FIELDVALUE_PK" PRIMARY KEY ("ID")
);

-- Table "DOCUMENT" for classes [com.myvdm.server.domain.Document]
CREATE TABLE "DOCUMENT"
(
    "ID" SERIAL,
    "NAME" varchar(255) NULL,
    "TYPE_ID" int8 NULL,
    "VERSION" int4 NULL,
    CONSTRAINT "DOCUMENT_PK" PRIMARY KEY ("ID")
);

-- Table "DOCUMENT_FIELD" for join relationship
CREATE TABLE "DOCUMENT_FIELD"
(
    "DOCUMENT_ID" int8 NOT NULL,
    "FIELDS_ID" int8 NULL
);

-- Constraints for table "INTEGERFIELDVALUE" for class(es) [com.myvdm.server.domain.IntegerFieldValue]
ALTER TABLE "INTEGERFIELDVALUE" ADD CONSTRAINT "INTEGERFIELDVALUE_FK1" FOREIGN KEY ("ID") REFERENCES "FIELDVALUE" ("ID") ;


-- Constraints for table "FIELD" for class(es) [com.myvdm.server.domain.Field]
ALTER TABLE "FIELD" ADD CONSTRAINT "FIELD_FK1" FOREIGN KEY ("VALUE_ID") REFERENCES "FIELDVALUE" ("ID") INITIALLY DEFERRED ;

CREATE INDEX "FIELD_N49" ON "FIELD" ("VALUE_ID");


-- Constraints for table "DOCUMENTTYPE" for class(es) [com.myvdm.server.domain.DocumentType]

-- Constraints for table "STRINGFIELDVALUE" for class(es) [com.myvdm.server.domain.StringFieldValue]
ALTER TABLE "STRINGFIELDVALUE" ADD CONSTRAINT "STRINGFIELDVALUE_FK1" FOREIGN KEY ("ID") REFERENCES "FIELDVALUE" ("ID") ;


-- Constraints for table "FIELDVALUE" for class(es) [com.myvdm.server.domain.FieldValue]
CREATE INDEX "FIELDVALUE_N49" ON "FIELDVALUE" ("FV");


-- Constraints for table "DOCUMENT" for class(es) [com.myvdm.server.domain.Document]
ALTER TABLE "DOCUMENT" ADD CONSTRAINT "DOCUMENT_FK1" FOREIGN KEY ("TYPE_ID") REFERENCES "DOCUMENTTYPE" ("ID") INITIALLY DEFERRED ;

CREATE INDEX "DOCUMENT_N49" ON "DOCUMENT" ("TYPE_ID");


-- Constraints for table "DOCUMENT_FIELD"
ALTER TABLE "DOCUMENT_FIELD" ADD CONSTRAINT "DOCUMENT_FIELD_FK1" FOREIGN KEY ("DOCUMENT_ID") REFERENCES "DOCUMENT" ("ID") INITIALLY DEFERRED ;

ALTER TABLE "DOCUMENT_FIELD" ADD CONSTRAINT "DOCUMENT_FIELD_FK2" FOREIGN KEY ("FIELDS_ID") REFERENCES "FIELD" ("ID") INITIALLY DEFERRED ;

CREATE INDEX "DOCUMENT_FIELD_N50" ON "DOCUMENT_FIELD" ("FIELDS_ID");

CREATE INDEX "DOCUMENT_FIELD_N49" ON "DOCUMENT_FIELD" ("DOCUMENT_ID");



------------------------------------------------------------------
-- Sequences and SequenceTables
