-- TODO how to set owner?

-- created schema name *must* be uppercase
SET search_path TO "MYVDM";
------------------------------------------------------------------
-- "ID" "NAME" "VERSION"
insert into "DOCUMENTTYPE" values (1, 'Revista', 1);
insert into "DOCUMENTTYPE" values (2, 'Volum Conferinta', 1);
insert into "DOCUMENTTYPE" values (3, 'Capitol Carte', 1);
insert into "DOCUMENTTYPE" values (4, 'Raport Tehnic', 1);
insert into "DOCUMENTTYPE" values (5, 'Teza Doctorat', 1);
insert into "DOCUMENTTYPE" values (6, 'Carte', 1);

-- "ID" "NAME" "TYPE_ID" "VERSION"
insert into "DOCUMENT" values (1, 'Paper #1', 1, 1);
insert into "DOCUMENT" values (2, 'Paper #2', 1, 1);

-- "ID" "VERSION" "FV"
insert into "FIELDVALUE" values (1, 1, 'string');
insert into "FIELDVALUE" values (2, 1, 'string');
insert into "FIELDVALUE" values (3, 1, 'integer');
insert into "FIELDVALUE" values (4, 1, 'string');
insert into "FIELDVALUE" values (5, 1, 'string');
insert into "FIELDVALUE" values (6, 1, 'integer');

-- "ID" "NAME" "VALUE_ID" "VERSION"
insert into "FIELD" values (1, 'Titlu', 1, 1);
insert into "FIELD" values (2, 'Autori', 2, 1);
insert into "FIELD" values (3, 'Nr Pagini', 3, 1);
insert into "FIELD" values (4, 'Titlu', 4, 1);
insert into "FIELD" values (5, 'Autori', 5, 1);
insert into "FIELD" values (6, 'Nr Pagini', 6, 1);

-- "FIELD_ID" "VALUE"
insert into "STRINGFIELDVALUE" values (1, 'Procedee de imbalsamare');
insert into "STRINGFIELDVALUE" values (2, 'Neluta Parastas');
insert into "STRINGFIELDVALUE" values (4, 'Descompunerea cadavrelor');
insert into "STRINGFIELDVALUE" values (5, 'Fane Cimitir');

-- "FIELD_ID" "VALUE" 
insert into "INTEGERFIELDVALUE" values (3, 127);
insert into "INTEGERFIELDVALUE" values (6, 12);

-- "DOCUMENT_ID" "FIELDS_ID"
insert into "DOCUMENT_FIELD" values (1, 1);
insert into "DOCUMENT_FIELD" values (1, 2);
insert into "DOCUMENT_FIELD" values (1, 3);
insert into "DOCUMENT_FIELD" values (2, 4);
insert into "DOCUMENT_FIELD" values (2, 5);
insert into "DOCUMENT_FIELD" values (2, 6);

-- GATA