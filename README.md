# myvdm is an AJAX-enabled, SP application 
It was designed and implemented from ground up for my dissertation thesis, in order to serve 
the needs of users looking to save and organize information about bibliographic papers. 
Currently there are plans to deploy it to West University of Timisoara's servers to be used.

# myvdm is a Virtual Document Management system
Examples of concrete documents: a contract, a receipt, information about a bibliographic paper.
myvdm abstracts away the concrete identity of documents, and views them as collections of 
fields. Every field can have a name and content. The content of the field can have any 
representation (string, integer, date). 
For example, a concrete document can have an 'author' field represented as a string, a 'date 
of creation' field represented as a date, a 'number of pages' field represented as an integer.
Documents can be saved, updated, deleted, while the structure and the content of the documents
is irrelevant. 
 
# myvdm is built using
* GWT 2.x
* JPA API with DataNucleus provider
* Spring
* postgreSql database
* Maven 3.x